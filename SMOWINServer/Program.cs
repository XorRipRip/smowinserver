﻿using System;
using System.Configuration;
using System.Net.Http;
using Microsoft.Owin.Hosting;
using Raven.Client.Embedded;
using SMOWINServer.Models;

namespace SMOWINServer
{

	public class Program
	{
		private static string dbName = ConfigurationManager.AppSettings ["dbName"] ?? "ParisDB";
		private static string msHost = ConfigurationManager.AppSettings ["host"] ?? "http://localhost:8087/";
		public static EmbeddableDocumentStore DbStore { get; set; }
		public static User AuthenticatedUser { get; set; }

		static void Main ( )
		{
			Start ( );
			Startup.AddUser ( );

			// Start OWIN host 
			using ( WebApp.Start<Startup> ( url: msHost ) )
			{
				// Create HttpCient and make a request to api/values 
				HttpClient client = new HttpClient();
				var response = client.GetAsync(msHost + "api/test/Get/5").Result;
				Console.WriteLine ( response );
				Console.WriteLine ( response.Content.ReadAsStringAsync ( ).Result );

				Console.ReadLine ( );
			}

			DbStore.Dispose ( );
		}

		private static void Start ( )
		{
			DbStore = Startup.ConfigureRavenDB ( dbName );
			DbStore.AfterDispose += ( s, e ) =>
			{
				Console.WriteLine ( "DBSTORE DISPOSED!" );
			};
		}
	}
}
