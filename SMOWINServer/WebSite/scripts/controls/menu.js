﻿$(document).ready(function () {
	$("#menu-toggle").click(function (e) {
		e.preventDefault();
		$("#wrapper").toggleClass("active");
	});

	$('#logoutBtn').on('click', function () {
		localStorage.removeItem('auth_access_token');
		localStorage.removeItem('auth_username');
		localStorage.removeItem('auth_token_expiration');

		window.location = 'index.html';
	});
});