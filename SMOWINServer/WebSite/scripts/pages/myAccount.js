﻿$(document).ready(function () {
	/*	*Menu-toggle*/
	$("#menu-toggle").click(function (e) {
		e.preventDefault();
		$("#wrapper").toggleClass("active");
	});

	/*Scroll Spy*/
	$('body').scrollspy({ target: '#spy', offset: 80 });

	/*Smooth link animation*/
	$('a[href*=#]:not([href=#])').click(function () {
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {

			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top
				}, 1000);
				return false;
			}
		}
	});

	var loginAPIUrl = '../../api/account/GetUserName';
	$.ajax({
		url: loginAPIUrl,
		type: 'GET',
		dataType: 'json',
		headers: { 'Authorization': 'Bearer ' + localStorage['auth_access_token'] },
		success: function (data) {
			var outputJSON = data;
			bootbox.alert({
				title: 'Hello',
				message: JSON.stringify(outputJSON)
			});
		},
		error: function (data) {
			var errJSON = JSON.parse(data.responseText);
			if (errJSON !== undefined) {
				errMessage = errJSON.ExceptionMessage;
			}

			bootbox.alert({
				title: 'Error',
				message: errJSON,
				backdrop: true
			});
		}
	});

	$('#menu').load('../controls/menu.html');
});