﻿$(document).ready(function () {
	$('#login-submit').on('click', function (e) {
		var loginAPIUrl = '../../token';
		var login = $('#username').val();
		var pass = $('#password').val();
		var loginModel =
		{
			grant_type: 'password',
			username: login,
			password: pass
		}

		$.ajax({
			url: loginAPIUrl,
			type: 'POST',
			contentType: "application/json",
			data: loginModel,
			success: function (data) {
				$('#login-submit').val('ok');
				bootbox.alert({
					message: data['access_token'],
					callback: function () { window.location = "pages/myAccount.html"; }
				});
				localStorage.setItem('auth_access_token', data['access_token']);
				localStorage.setItem('auth_username', data['username']);
				localStorage.setItem('auth_token_expiration', data['expires']);
			},
			error: function (data) {
				bootbox.alert(data.statusText);
			}
		});

		return false;
	});
});
