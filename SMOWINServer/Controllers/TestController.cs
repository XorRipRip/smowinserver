﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace SMOWINServer.Controllers
{
	public class TestController : ApiController
	{
		// GET api/values 
		[HttpGet]
		public async Task<IHttpActionResult> Get ( int id )
		{
			Console.WriteLine ( $"input query - {id}" );

			return Ok ( new
			{
				total = "value1",
				users = "user1"
			} );
		}

		[HttpGet]
		public HttpResponseMessage MyAccount ( )
		{
			//Console.WriteLine ( "try go to MyAccount" );
			return Request.CreateResponse ( HttpStatusCode.OK, new Uri ( "Pages/myAccount.html", UriKind.Relative ) );
		}

		// GET api/values 
		//public async Task<IHttpActionResult> Get ( )
		//{
		//	Console.WriteLine ( "input query" );

		//	return Ok ( new
		//	{
		//		total = "value1",
		//		users = "user1"
		//	} );
		//}

		//// GET api/values/5 
		//public string Get ( int id )
		//{
		//	return "value";
		//}

		//// POST api/values 
		//public void Post ( [FromBody]string value )
		//{
		//}

		//// PUT api/values/5 
		//public void Put ( int id, [FromBody]string value )
		//{
		//}

		//// DELETE api/values/5 
		//public void Delete ( int id )
		//{
		//}
	}
}