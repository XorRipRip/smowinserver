﻿using System.Web.Http;
using Raven.Client;
using System.Linq;
using SMOWINServer.Helpers;
using System;
using System.Security.Claims;
using SMOWINServer.VewModels;
using SMOWINServer.Models;

namespace SMOWINServer.Controllers
{
	public class AccountController : ApiController
	{
		IDocumentStore m_db = null;

		public AccountController ( )
		{
			m_db = Program.DbStore;
		}

		[HttpGet]
		[Authorize]
		[ActionName ( "Logout" )]
		public IHttpActionResult Logout ( )
		{
			SignInHelper.SignOut ( Request );
			return Ok ( new Uri ( "../../Pages/index.html", UriKind.Relative ) );
		}

		[HttpGet]
		[Authorize]
		[ActionName ( "GetUserName" )]
		public IHttpActionResult GetUserName ( )
		{
			var identity = User.Identity as ClaimsIdentity;
			var sEmail = identity.Claims.Where ( c => c.Type == ClaimsEnum.Email.ToString() ).Single ( ).Value;
			var sName = identity.Claims.Where ( c => c.Type == ClaimsEnum.Name.ToString()).Single().Value;
			var sLevel = identity.Claims.Where(c => c.Type == ClaimsEnum.Level.ToString()).Single().Value;
			var sExperience = identity.Claims.Where(c => c.Type == ClaimsEnum.Level.ToString()).Single().Value;

			return Ok ( new UserModel ( ) { Email = sEmail, Name = sName, Level = Convert.ToInt32 ( sLevel ), Experience = Convert.ToInt32 ( sExperience ) } );
		}
	}
}
