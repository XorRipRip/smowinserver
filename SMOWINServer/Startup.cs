﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Microsoft.Owin;
using Owin;
using Raven.Abstractions.Data;
using Raven.Client.Embedded;
using Raven.Database.Server;
using Raven.Client;
using SMOWINServer.Models;
using Microsoft.Owin.FileSystems;
using Microsoft.Owin.StaticFiles;
using SMOWINServer.Helpers;
using Microsoft.Owin.Security.OAuth;

namespace SMOWINServer
{
	public class Startup
	{
		// This code configures Web API. The Startup class is specified as a type
		// parameter in the WebApp.Start method.
		public void Configuration ( IAppBuilder appBuilder )
		{
			// Configure Web API for self-host. 
			HttpConfiguration config = new HttpConfiguration();
			config.Routes.MapHttpRoute (
				name: "DefaultApi",
				routeTemplate: "api/{controller}/{action}/{id}",
				defaults: new { id = RouteParameter.Optional }
			);
			config.MapHttpAttributeRoutes ( );

			var OAuthOptions = new OAuthAuthorizationServerOptions
			{
				TokenEndpointPath = new PathString("/token"),
				Provider = new ApplicationOAuthServerProvider(),
				AccessTokenExpireTimeSpan = TimeSpan.FromDays(14),
				//RefreshTokenProvider = new SimpleRefreshTokenProvider(),

                // Only do this for demo!!
                AllowInsecureHttp = true
			};
			appBuilder.UseOAuthAuthorizationServer ( OAuthOptions );
			appBuilder.UseOAuthBearerAuthentication ( new OAuthBearerAuthenticationOptions ( ) );
			appBuilder.UseCors ( Microsoft.Owin.Cors.CorsOptions.AllowAll );
			appBuilder.UseWebApi ( config );

			var physicalFileSystem = new PhysicalFileSystem ( "../../WebSite" );
			var options = new FileServerOptions()
			{
				EnableDefaultFiles = true,
				FileSystem = physicalFileSystem,
				EnableDirectoryBrowsing = true
			};
			options.StaticFileOptions.FileSystem = physicalFileSystem;
			options.DefaultFilesOptions.DefaultFileNames = new []
			{
				"Pages/index.html"
			};
			appBuilder.UseFileServer ( options );
		}

		public static EmbeddableDocumentStore ConfigureRavenDB ( string dbName )
		{
			Console.WriteLine ( "ConfigureRavenDB started..." );

			if ( Program.DbStore != null )
			{
				try
				{
					Program.DbStore.Dispose ( );
				}
				catch ( Exception ex )
				{
					Console.WriteLine ( "DBSTORE DISPOSE ERROR: " + ex.Message + ";" + ex.StackTrace );
				}
			}

			try
			{
				using ( var tempStore = new EmbeddableDocumentStore ( )
				{
					DataDirectory = "Data/System"
				}.Initialize ( ) )
				{
					var names = tempStore.DatabaseCommands.GlobalAdmin.GetDatabaseNames(10);
					Console.WriteLine ( string.Join ( ",", names ) );
					if ( !names.Contains ( dbName ) )
					{
						Console.WriteLine ( "creating database..." );
						tempStore.DatabaseCommands.GlobalAdmin.CreateDatabase ( new DatabaseDocument ( )
						{
							Id = dbName,
							Settings = new Dictionary<string, string> ( )
							{
								{"Raven/DataDir", @"~\Data" + dbName},
								{"Raven/ActiveBundles", "DocumentExpiration"},
								{"Raven/StorageTypeName", "esent"}
							}
						} );
					}
				}

				var dbStore = new EmbeddableDocumentStore()
				{
					DefaultDatabase = dbName,
					RunInMemory = false,
					UseEmbeddedHttpServer = true
				};
				dbStore.Configuration.DataDirectory = "Data/System";
				dbStore.Configuration.RunInMemory = false;
				dbStore.Configuration.Port = 8090;
				dbStore.Initialize ( );
				NonAdminHttp.EnsureCanListenToWhenInNonAdminContext ( 8090 );

				return dbStore;

			}
			catch ( Exception ex )
			{
				Console.WriteLine ( "DBSTORE CREATION ERROR: " + ex.Message + ";" + ex.StackTrace );
				return null;
			}
			finally
			{
				Console.WriteLine ( "ConfigureRavenDB completed..." );
			}
		}

		public static void AddUser ( )
		{
			var dbStore = Program.DbStore;
			string sLogin = "my@mail.ru";

			using ( IDocumentSession session = dbStore.OpenSession ( ) )
			{
				List<User> lsUsers = session.Query<User>().Where(u => u.Email == sLogin).ToList();
				if ( lsUsers.Count > 0 )
				{
					Console.WriteLine ( "AddUser - Skip" );
					return;
				}

				string sNewId = Guid.NewGuid().ToString();
				string sNewName = "First";

				User oUser = new User ( )
				{
					Id = sNewId,
					//CreationTS = DateTime.UtcNow,
					Email = sLogin,
					Password = "12345",
					UserName = sNewName,
					Level = 1,
					Experience = 0,
					Claims = new Dictionary<string, string>()
					{
						{ ClaimsEnum.Role.ToString ( ), "Admin" },
						{ ClaimsEnum.Name.ToString ( ), sNewName },
						{ ClaimsEnum.Email.ToString ( ), sLogin },
						{ ClaimsEnum.Level.ToString ( ), 1 + "" },
						{ ClaimsEnum.Experience.ToString ( ), 0 + "" }
					}
				};

				session.Store ( oUser );
				session.SaveChanges ( );
				Console.WriteLine ( "AddUser - Done" );
			}

			using ( var ctx = dbStore.OpenSession ( ) )
			{
				try
				{
					//.Where ( u => u.Email == "my@mail.ru" )
					var usrs = ctx.Query<User> ( ).ToList();

				}
				catch ( Exception ex ) { }
			}
		}
	}
}
