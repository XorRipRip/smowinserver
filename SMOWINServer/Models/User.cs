﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace SMOWINServer.Models
{
	public class User
	{
		public User ( )
		{

		}

		public string Id { get; set; } = string.Empty;
		public string UserName { get; set; }
		public string Email { get; set; }
		public string Phone { get; set; }
		public string Password { get; set; }
		public string VKId { get; set; }
		public int Level { get; set; } = 1;
		public int Experience { get; set; } = 0;
		public DateTime CreationTS { get; set; }
		public List<string> HistoryIdList { get; set; }
		public Dictionary<string, string> Claims { get; set; }
	}
}
