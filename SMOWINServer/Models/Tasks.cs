﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMOWINServer.Models
{
	class Tasks
	{
		public string ID { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public int RequiredLevel { get; set; }
		public DateTime CreateTS { get; set; }
	}
}
