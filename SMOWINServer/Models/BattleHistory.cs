﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMOWINServer.Models
{
	class BattleHistory
	{
		public string Id { get; set; }
		public string FirstUserId { get; set; }
		public string SecondUserId { get; set; }
		public string TaskId { get; set; }
		public DateTime StartTimeTS { get; set; }
		public DateTime EndTimeTS { get; set; }
		public string WinnerId { get; set; }
		public int FirstLikes { get; set; }
	}
}
