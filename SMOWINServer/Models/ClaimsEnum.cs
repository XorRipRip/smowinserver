﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMOWINServer.Models
{
	public enum ClaimsEnum
	{
		Role,
		Name,
		Email,
		Level,
		Experience
	}
}
