﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMOWINServer.Models
{
	class Battle
	{
		public string Id { get; set; }
		public string TaskId { get; set; }
		public string FirstOpponentId { get; set; }
		public string SecondOpponentId { get; set; }
		public int FirstVotes { get; set; } = 0;
		public int SecondVotes { get; set; } = 0;
		public DateTime StartBattleTS { get; set; }
		public DateTime EndBattleTS { get; set; }
	}
}
