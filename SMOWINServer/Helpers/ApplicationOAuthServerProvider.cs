﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin.Security.OAuth;
using Raven.Client;
using SMOWINServer.Models;

namespace SMOWINServer.Helpers
{
	class ApplicationOAuthServerProvider : OAuthAuthorizationServerProvider
	{
		IDocumentStore m_db = null;

		public ApplicationOAuthServerProvider ( )
		{
			m_db = Program.DbStore;
		}

		public override async Task ValidateClientAuthentication (
			OAuthValidateClientAuthenticationContext context )
		{
			// This call is required...
			// but we're not using client authentication, so validate and move on...
			await Task.FromResult ( context.Validated ( ) );
		}

		public override async Task GrantResourceOwnerCredentials (
			OAuthGrantResourceOwnerCredentialsContext context )
		{
			context.OwinContext.Response.Headers.Add ( "Access-Control-Allow-Origin", new [] { "*" } );

			// Retrieve user from database:
			var user = GetUserByLogin ( m_db, context.UserName );

			// Validate user/password:
			if ( user == null || !String.Equals ( user.Password, context.Password ) )
			{
				context.SetError (
					"invalid_grant", "The user name or password is incorrect." );
				context.Rejected ( );
				return;
			}

			// Add claims associated with this user to the ClaimsIdentity object:
			var identity = new ClaimsIdentity ( context.Options.AuthenticationType );
			foreach ( var userClaim in user.Claims )
			{
				identity.AddClaim ( new Claim ( userClaim.Key, userClaim.Value ) );
			}

			context.Validated ( identity );
		}

		private User GetUserByLogin ( IDocumentStore dbStore, string sLogin )
		{
			using ( var ctx = dbStore.OpenSession ( ) )
			{
				List<User> lsUsers = ctx.Query<User>().Where(u => u.Email == sLogin).ToList();
				if ( lsUsers.Count == 1 )
				{
					return lsUsers [ 0 ];
				}

				return null;
			}
		}

	}
}
