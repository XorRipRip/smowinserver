﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.DataProtection;
using SMOWINServer.Models;
using SMOWINServer.ViewModels;

namespace SMOWINServer.Helpers
{
	public class SignInHelper
	{
		public static void SignIn ( HttpRequestMessage request, User oUser, LoginModel userModel, bool isPersistent = true )
		{
			var context = request.GetOwinContext().Authentication;

			var claims = new []
			{
				new Claim (ClaimTypes.Name, "myUser"),
				new Claim ( ClaimTypes.Email, "my@mail.ru")
			};

			//var identity = new ClaimsIdentity(claims, "ApplicationCookie");
			var identity = new ClaimsIdentity ( claims, DefaultAuthenticationTypes.ApplicationCookie );

			context.SignIn ( new AuthenticationProperties { IsPersistent = true }, identity );
		}

		public static void SignOut ( HttpRequestMessage request )
		{
			request.GetOwinContext ( ).Authentication.SignOut ( );
		}
	}
}
