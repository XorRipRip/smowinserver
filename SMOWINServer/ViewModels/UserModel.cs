﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMOWINServer.VewModels
{
	public class UserModel
	{
		public string Email { get; set; }
		public string Name { get; set; }
		public int Level { get; set; } = 1;
		public int Experience { get; set; } = 0;
	}
}
