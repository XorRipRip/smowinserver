﻿using System.ComponentModel.DataAnnotations;

namespace SMOWINServer.ViewModels
{
	public class LoginModel
	{
		[Required]
		[EmailAddress]
		[Display ( Name = "Login" )]
		public string Email { get; set; }

		[Required]
		[DataType ( DataType.Password )]
		[Display ( Name = "Password" )]
		public string Password { get; set; }

		//[Display ( Name = "Remember me?" )]
		//public bool RememberMe { get; set; }
	}
}
