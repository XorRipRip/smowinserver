Selfie Master OWIN Server
=====================
Work Plan
-----------------------------------
### Server
* Forming ajax objects from JSON ( JS objects prototype-s ? )
### WebSite
* overall site-plan
* page sketches
#### Menu items
* Account
level, percent of victory\looses, experience until next level...
* Gallery
* Battle
start random new, start new by task, view current
* Tasks
count of opened tasks, unopened tasks... Possible to chose task and start
* Settings ?
change name, password, avatar...
* Exit
Knowledge Base
-----------------------------------
* Work Url: http://localhost:8087/index.html
* RavenDB port: 8090
* WebSite port: 8087
* MD styles: http://webdesign.ru.net/article/pravila-oformleniya-fayla-readmemd-na-github.html
### On problems with connection
* netsh http delete urlacl http://+:8087/
* netsh http show urlacl > 1

***Initial version of this manual: 20.09.17***